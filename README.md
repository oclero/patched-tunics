# About Patched Tunics!

 * Pached Tunics! is a fork of [Tunics!](https://tunics.legofarmen.se/), a [roguelike-like](https://en.wikipedia.org/wiki/Roguelike) action role playing game based on the look and mechanics of 'The legend of Zelda – A link to the past' for SNES running on the [Solarus](http://solarus-games.org) game engine.

 * Patched Tunics! requires Solarus 1.6.x.

 * Patched Tunics! consists in part of materials under various free and open-source and public licenses, and in part of unlicensed materials under fair use. Open Patched Tunics! with [Solarus Quest Editor](https://gitlab.com/solarus-games/solarus-quest-editor) for details.


# Play Patched Tunics!

## Windows
 
 1. Install the latest Solarus 1.6.x engine from http://www.solarus-games.org/engine/download/.

 2. Download the `quest-package` artifact (zip) for Patched Tunics!

 3. Extract the contents of the zip where you installed Solarus.
 
 4. Run solarus-run.exe


## Ubuntu

 1. Install the latest Solarus 1.6.x engine from http://www.solarus-games.org/engine/download/.

 2. Download the `quest-package` artifact (zip) for Patched Tunics!

 3. Extract the contents of the zip into your home directory.

 4. Run `solarus-run $HOME/patched-tunics.solarus`


## OS X

 1. Install the latest Solarus 1.6.x engine from http://www.solarus-games.org/engine/download/.

 2. Download and extract the `quest-package` artifact (zip) for Patched Tunics!

 3. Copy the Solarus-run application from the Solarus bundle to the Patched Tunics! directory.
 
 4. Run the Solarus-run application.
